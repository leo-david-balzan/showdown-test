export default interface Article {
  id: number;
  content: string;
}

export default interface ArticleResponse {
  data: Article[];
}
