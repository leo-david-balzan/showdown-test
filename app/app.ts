import axios from "axios";
import express = require("express");
import * as showdown from "showdown";
import ArticleResponse from "./model/article";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

// Create a new express application instance
const app: express.Application = express();

const getLogo = () => {
  return '<img src="https://res.cloudinary.com/casinogrounds/image/upload/reactapp/CGlogo-Skely.svg"/>';
};

const extensions = () => {
  const logoExt = {
    type: "lang",
    regex: /\!\[cg-logo\]/g,
    replace: getLogo()
  };

  const neptuneExt = {
    type: "lang",
    regex: /\!\[neptune-media\]\[([\s\S]+)\]/g,
    replace: (tag: string, params: string) => {
      // removes spaces then transforms key value , seperated pairs into an object
      const kvPairs = params
        .replace(/\s/g, "")
        .split(/,(?=[^,]+=)/)
        .map(s => s.split("="));

      // target strucutre
      const media = {
        id: "",
        width: 30,
        height: 30,
        allowfullscreen: true,
        frameborder: 0
      };

      kvPairs.forEach(([key, value]) => (media[key] = value));

      return `<iframe src="https://www.neptuneplatform.net/ws/player/${media.id}/" 
      height="${media.height}" width="${media.width}" allowfullscreen="${media.allowfullscreen}" frameborder="${media.frameborder}"></iframe>`;
    }
  };

  return [logoExt, neptuneExt];
};

const converter = new showdown.Converter({
  emoji: true,
  extensions: [extensions]
});

app.get("/articles/:id", (req, res) => {
  const id: number = Number(req.params.id);
  getArticle(id).then(art => {
    art.content = converter.makeHtml(art.content);
    res.json(art);
  });
});

app.get("/articles?:query", (req, res) => {
  const query: string = req.url.split("?")[1];
  getArticles(query).then(art => {
    art.content = converter.makeHtml(art.content);

    res.send(art.content);
  });
});

app.listen(3000, () => {
  console.log("Example app listening on port 3000!");
});

const getArticle = async (id: number) => {
  const url = `https://cms.casinogrounds.local/articles?id=${id}`;
  const articles = await axios.get<any, ArticleResponse>(url);
  return articles.data[0];
};

const getArticles = async (query: string) => {
  const url = `https://cms.casinogrounds.local/articles?${query}`;
  const articles = await axios.get<any, ArticleResponse>(url);
  return articles.data[0];
};
